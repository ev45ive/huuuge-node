import { Entity, PrimaryGeneratedColumn, Column, OneToOne } from "typeorm";
import { UserProfile } from "./UserProfile";

@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id?: number;

    @Column()
    firstName: string = '';

    @Column()
    lastName: string = '';

    // @Column()
    // age?: number;

    @OneToOne(type => UserProfile, {
        lazy: true,
        onDelete: 'CASCADE',
    })
    profile?: Promise<UserProfile>

}
