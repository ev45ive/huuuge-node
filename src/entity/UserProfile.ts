import { Column, Entity, OneToOne, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";
import { User } from "./User";

@Entity()
export class UserProfile {
  // @PrimaryColumn()

  @PrimaryGeneratedColumn()
  id?: number;

  @OneToOne(type => User, {
    eager: true,
    // primary: true,
  })
  user?: User

  @Column()
  bio: string = '';

}
