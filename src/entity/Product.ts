import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Category } from "./Category";

@Entity()
export class Product {

  @PrimaryGeneratedColumn()
  id?: number

  @Column()
  name: string = ''

  @Column()
  price: number = 0

  @ManyToOne(type => Category, {})
  category?: Category
}
