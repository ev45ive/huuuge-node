import { Router } from "express";
import { categoriesService } from '../services'

export const categoriesRoutes = Router()
  .get("/", async (req, res) => {
    res.json(await categoriesService.getCategories());
  })
  .get("/:categoryId", async (req, res) => {
    const {categoryId} = req.params;
    res.json(await categoriesService.getCategory(+categoryId));
  })
  .post('/', async (req, res) => {
    const {id, name} = req.body;
    if(await categoriesService.saveCategory({id: +id, name})) {
      res.sendStatus(201);
      return;
    }

    res.sendStatus(409);
  })
  .put('/', async (req, res) => {
    const {id, name} = req.body;
    if(await categoriesService.updateCategory({id: +id, name})) {
      res.sendStatus(201);
      return;
    }

    res.sendStatus(409);
  });
