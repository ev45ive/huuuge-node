let wishListElements = [
    {
        productId: 1,
    },
    {
        productId: 2,
    },
    {
        productId: 3,
    }
];

export class WishlistService {
    async remove(productId: number) {
        wishListElements = wishListElements.filter((el) => {
            return el.productId != productId;
        });
    }
    async getAll() {
        return wishListElements;
    }
    async getWishlistElement(productId: number) {
        return wishListElements.filter((el) => {
            return el.productId == productId;
        });
    }
    async save(product: { productId: any; }) {
        wishListElements.push(product);
    }

}