import { UsersService } from "./users";
import { CategoriesService } from "./categories";
import { WishlistService } from './wishlist';

export const usersService = new UsersService()
export const categoriesService = new CategoriesService()
export const wishlistService = new WishlistService()

