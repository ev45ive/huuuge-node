import { mockUsers } from "./mockUsers";
// import { expect } from 'chai' 
// index.d.ts +  -r chai/register-expect 

import { UsersService } from "./users";

describe('Users Service', () => {
  let users: UsersService

  beforeEach(() => {
    users = new UsersService()
  })

  it('is created', () => {
    expect(2 + 2).to.equal(4)
  });


  it('gets users', async () => {
    // const result = await users.getUsers()
    // console.log(result, mockUsers)
    // expect(mockUsers).to.deep.equals(result)
    await expect(users.getUsers()).to.eventually.deep.equals(mockUsers)/* .notify(done) */

    // return expect(users.getUsers()).to.eventually.deep.equals(mockUsers)/* .notify(done) */
  })

});