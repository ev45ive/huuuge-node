import "reflect-metadata";
import { createConnection } from "typeorm";
import { Product } from "../entity/Product";
import { Category } from "../entity/Category";

createConnection().then(async connection => {
  console.log("Inserting a new categories into the database...");
  // await connection.createQueryBuilder()
  //   .delete()
  //   .from(Category)
  //   .where()
  //   .execute()
  console.log('Deleted categories')

  const categories = Array.apply(null, Array(40))
    .map(() => new Category())
    .map((category: Category, i) => {
      category.name = `Category ${++i}`
      return category;
    });

  const p1 = new Product()
  p1.name = 'Product 1'
  const p2 = new Product()
  p2.name = 'Product 2'

  // categories[0].products = [p1, p1]
  p1.category = categories[0]

  await connection.manager.save(categories[0]);
  await connection.manager.save(p1)
  console.log("Saved a new categories with ids: " + JSON.stringify(categories.map(x => x.id)));

  console.log("Loading categories from the database...");
  const loadedCategories = await connection.manager.findByIds(Category, [categories[0].id!], {
    // loadEagerRelations:true,
    // relations:['products']
  });
  console.log("Loaded categories: ", loadedCategories);

  console.log(p1, p2, loadedCategories[0])
  const loaded = await loadedCategories[0].products;
  console.log(p1, p2, loadedCategories[0], loaded)


}).catch(error => console.log(error));
