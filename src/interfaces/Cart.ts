
declare global {
  namespace Express {
    interface Session {
      cart: {
        items: Cart.Item[];
        total: number;
      };
    }
  }
}


export namespace Cart {
  /**
   * Cart Line item
   * product_id
   * amount
   */
  export interface Item { }
  export interface ResponseDTO {
    items: Item[];
    total: number;
  }

  export interface ItemParams {
    lineItemId: string;
  }

  export interface ChangeItemRequestDTOBase {
    productId: number;
    amount: number;
  }
  export interface AddItemRequestDTO extends ChangeItemRequestDTOBase { }
  export interface UpdateItemRequestDTO extends ChangeItemRequestDTOBase { }
}