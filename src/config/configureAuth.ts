import passport from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';

// http://www.passportjs.org/packages/passport-jwt/
// passport.use(new JWTStrategy(...))

// app.post('/profile', passport.authenticate('jwt', { session: false }),(req, res) => {  res.send(req.user.profile);

passport.use(new LocalStrategy(
  function (username, password, done) {

    if (Math.random() > 0.9) {
      return done(new Error('Cannot conntect to database'));
    }

    if (username == 'admin' && password == 'pa55word') {
      const userData = {
        username: 'admin', id: 1
      };
      return done(null, userData);
    } else {
      return done(null, false, { message: 'Invalid username od password' });
    }
  }
));

passport.serializeUser(function (user, done) {
  done(null, user);
});
passport.deserializeUser(function (user, done) {
  // User.findById(id, function(err, user) {
  done(null, user);
  // });
});
