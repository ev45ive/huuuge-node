module.exports = {
  apps : [
      {
        name: "app",
        script: "./dist/index.js",
        watch: true,
        env: {
          "NODE_ENV": "development",
          "PORT":8080,
          "HOST":"localhost"
        },
        instances : "max",
        exec_mode : "cluster"
      }
  ]
}